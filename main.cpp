﻿// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>
#include <shape.hpp>
#include <vector>

using namespace std::chrono_literals;

int main()
{
    //генератор случайных чисел 
    srand(time(0));//инициализируется текущем временем 


    const int width = 800;
    const int height = 600;
    const int N = 50;//кол-во фигурок

    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(width, height), L"Пархающие прямоугольники!");

    // 1 
    std::vector<kr::Rectangle*> rectangles; //rectangles - название вектора

    for (int i = 0; i <= width; i += width / N)//расставляем фигурки с промежутками ( i<800-длина окна)

        rectangles.push_back(new kr::Rectangle( i, 600,  10, 20, rand() % 5 + 1)); // a = rand() дает от 0 до 32767 -(разные скорости) , a % 5 + 1, i - x ,600 - у,10  - ширина прямоугольника,20 - высота


    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }

        // 2 Обработка - двигаем прямоугольники 
        for (const auto& rectangle : rectangles)
        {
            rectangle->Move();
            //если y<0 ,тогда прямоугольник останавливается в заданной позиции
            if (rectangle->GetY() < 0)
            {
                rectangle->Stop();
       
            }
        }

        // Очистить окно от всего
        window.clear();

        // 3
        // Перемещение фигуры в буфер
        for (const auto& rectangle : rectangles)
            window.draw(*rectangle->Get());

        // Отобразить на окне все, что есть в буфере
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    for (const auto& rectangle : rectangles)
        delete rectangle;
    rectangles.clear();

    return 0;
}